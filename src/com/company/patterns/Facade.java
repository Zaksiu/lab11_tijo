package com.company.patterns;

public class Facade {

    public interface Light {
        void on();

        void off();
    }

    public class Lights implements Light {
        public Lights() {
        }

        @Override
        public void on() {

            System.out.println("Light on.");

        }

        @Override
        public void off() {

            System.out.println("Light off.");

        }

    }
}
