package com.company;


import com.company.patterns.Facade;

class MainApi{

    private Facade.Light lights = new Facade().new Lights();



    public void lightOff(){

        System.out.println("------ LIGHT OFF -----");
        lights.off();
    }

    public void lightOn(){

        System.out.println("------ LIGHT ON -----");
        lights.on();
    }
}
