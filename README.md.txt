Fasada (wzorzec projektowy)

Fasada � wzorzec projektowy nale��cy do grupy wzorc�w strukturalnych. S�u�y do ujednolicenia dost�pu do z�o�onego systemu poprzez wystawienie uproszczonego, uporz�dkowanego interfejsu programistycznego, kt�ry u�atwia jego u�ycie.


Przyk�ad

Przyk�adem u�ycia wzorca fasady mo�e by� aplikacja bankomatowa, kt�ra musi wchodzi� w interakcj� z systemem bankowym. Skoro aplikacja bankomatowa wykorzystuje tylko niewielk� cz�� mo�liwo�ci systemu bankowego (autoryzacja karty, sprawdzenie stanu konta, wyp�ata i ew. wp�ata), to mo�na zastosowa� obiekt fasady, kt�ry zas�oni przed zewn�trznymi aplikacjami skomplikowan� struktur� wewn�trzn� systemu bankowego. Upraszcza to pisanie aplikacji na bankomaty, a jednocze�nie zapewnia lepsze bezpiecze�stwo systemu bankowego.


Budowa

Klient komunikuje si� z systemem poprzez fasad�, kt�ra w jego imieniu wykonuje niezb�dne operacje na z�o�onym systemie. To, czy klient posiada tak�e bezpo�redni dost�p do systemu, le�y w gestii programisty implementuj�cego wzorzec, poniewa� mo�liwe jest wykorzystanie go do podzia�u systemu na warstwy, gdzie fasady s�u�� do uproszczenia i ujednolicenia komunikacji.


Konsekwencje stosowania

Zalety:

- Jeden punkt styku systemu z naszym rozbudowanym systemem. Mo�emy dokonywa� zmian bez konieczno�ci informowania systemu Klienta o tym �e co� zmodyfikowali�my
- W przypadku monolitycznego systemu, kt�rego chcemy przerobi� na mikroserwisy, na wej�ciu monolita mo�na postawi� fasad�. Mo�emy wtedy spokojnie prze��cza� poszczeg�lne elementy naszego systemu bez obawy, �e co� u klienta si� popsuje.
- Mo�emy w jednym miejscu dokona� transformacji danych, logowania co si� dzieje
- Fasada mo�e by� r�wnie� mechanizmem filtruj�cym, kt�ry zatrzyma nie zautoryzowany ruch

Wady:

- Jak przestanie dzia�a� serwis, kt�ry stanowi fasad�, to Klient odnosi wra�enie �e pada ca�y system. Ale w praktyce mo�na si� przeciw temu zabezpieczy� stosuj�c load balancery, proxy typu High Availability, itd.